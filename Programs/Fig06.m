% SuperZoomer for DNA image

% Get the data and normalize
load('../Data/dataDNAmapping_subset.mat');
Y0 = dataDNAmapping;
Y0 = (Y0 - min(Y0(:))) / (max(Y0(:)) - min(Y0(:)));
Y = Y0;

% Show original image
close all
subplot(1, 3, 1)
imagesc(Y0)
C = flipud(colormap(gray));
colormap(C)
title('Original')

% Show the selection
subplot(1, 3, 2)
imagesc(Y)
colormap(C)
title('Selection')

% Compute PS, set fsup to super-resolution factor
fsup = 8;
[n1 n2] = size(Y);
p1 = fsup * n1;
p2 = fsup * n2;
sigma = 0.8 * fsup;
S1a = psfmat(p1, sigma);
S2a = psfmat(p2, sigma);
S1 = kron(eye(n1), ones(1, fsup)) * S1a;
S2 = kron(eye(n2), ones(1, fsup)) * S2a;

% Initialize 
U = S1' * Y * S2;
G1 = S1' * S1;
G2 = S2' * S2;
kappa = 0.01;
lambda = 100;
delta = 0.001;
X = CG2(U, G1, G2, kappa, lambda, delta);

% Show the zoomed-in image
subplot(1, 3, 3)
imagesc(X)
colormap(C)
title('Zoomed')


