% Illustrate basic CG in two dimensions

load ../Data/dataDNAmapping.mat
Y = dataDNAmapping(51:100, 51:100, 1);
Y = (Y - min(Y(:))) / (max(Y(:)) - min(Y(:)));

% Compute PSF
[m1 m2] = size(Y);
fsup = 4;
n1 = fsup * m1;
n2 = fsup * m2;
sigma = 0.8* fsup;
S1a = psfmat(n1, sigma);
S2a = psfmat(n2, sigma);
S1a = psfmat(n1, sigma);
S2a = psfmat(n2, sigma);
S1 = kron(eye(m1), ones(1, fsup)) * S1a;
S2 = kron(eye(m2), ones(1, fsup)) * S2a;

% Prepare components of the linear system
U = S1' * Y * S2;
G1 = S1' * S1;
G2 = S2' * S2;
kappa = 0.001;

X1 = CG2(U, G1, G2, kappa, 0, 0.001);
X2 = CG2(U, G1, G2, kappa, 1, 0.001);

figure(1)
subplot(2, 2, 1)
imagesc(Y);
C = flipud(colormap(gray));
colormap(C)
title('Selection')

subplot(2, 2, 2)
imagesc(X1);
C = flipud(colormap(gray));
colormap(C)
title('Zoomed')

subplot(2, 2, 3)
imagesc(X2);
C = flipud(colormap(gray));
colormap(C)
title('Zoomed with roughness penalty')

subplot(2, 2, 4)
imagesc(-X1);
C = flipud(colormap(gray));
colormap(C)
title('Zoomed with roughness penalty')





