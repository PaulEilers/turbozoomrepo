% SuperZoomer for DNA image, with selection of ROI

% Get the data and normalize
load('../Data/Gattacell.mat');
Y0 = pxl128;
Y2 = pxl512;
Y0 = (Y0 - min(Y0(:))) / (max(Y0(:)) - min(Y0(:)));

% Show original image
close all
subplot(2, 2, 1)
imagesc(Y0)
C = flipud(colormap(gray));
colormap(C)
title('Original')

% Select a rectangle
i1 = 91;
i2 = 110;
j1 = 61;
j2 = 80;
Y = Y0(j1:j2, i1:i2);
hold on
plot ([i1, i1, i2, i2, i1], [j1, j2, j2, j1, j1], 'b')
hold off

% Renormalize selection
Y = (Y - min(Y(:))) / (max(Y(:)) - min(Y(:)));

% Show the selection
subplot(2, 2, 2)
imagesc(Y)
colormap(C)
title('Selection')

% Compute PS, set fsup to super-resolution factor
fsup = 4;
[n1 n2] = size(Y);
p1 = fsup * n1;
p2 = fsup * n2;
sigma = 0.05* fsup;
S1a = psfmat(p1, sigma);
S2a = psfmat(p2, sigma);
S1 = kron(eye(n1), ones(1, fsup)) * S1a;
S2 = kron(eye(n2), ones(1, fsup)) * S2a;

% Initialize 
U = S1' * Y * S2;
G1 = S1' * S1;
G2 = S2' * S2;
kappa = 0.0001;
lambda = 0.1;
delta = 0.00001;
X = CG2(U, G1, G2, kappa, lambda, delta);

% Correct range and baseline
rlambda = 100;
clambda = rlambda;
rpars = [30 2 rlambda 1];
cpars = rpars;
pars = [rpars; cpars];
Fhi = asypsp2(X, 0.99, pars);
Flo = asypsp2(X, 0.01, pars);
S = max(Fhi - Flo, 0.0);
G = (X - Flo) ./ S; 

% Show the zoomed-in image
subplot(2, 2, 3)
imagesc(X)
colormap(C)
title('Zoomed')

% Show the zoomed-in image
subplot(2, 2, 4)
Y3 = Y2((4 * j1):(4 * j2), (4 * i1) :(4 * i2));
imagesc(Y3)
colormap(C)
title('High resolution image')


 