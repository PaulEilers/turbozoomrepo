function C = rowtens(A, B);
% Compute (flattened) tensor products per row of A and B
na = size(A, 2);
nb = size(B, 2);
ea = ones(1, na);
eb = ones(1, nb);
C = kron(A, eb) .* kron(ea, B);
