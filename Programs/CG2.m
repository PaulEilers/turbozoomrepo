function [X, it] = CG2(U, G1, G2, kappa, lambda, delta)

R = U;
P = R;
n1 = size(G1, 2);
n2 = size(G2, 2);
X = zeros(n1, n2);
D1 = diff(eye(n1));
D2 = diff(eye(n2));
V1 = lambda * D1' * D1;
V2 = lambda * D2' * D2;

for it = 1:100
  Q = G1 * P * G2 + kappa * P + V1 * P + P * V2';
  alpha = sum(R(:) .^ 2) / sum(P(:) .* Q(:));
  X = X + alpha * P;
  Rnew = R - alpha * Q;
  rs1 = sum(R(:) .^ 2);
  rs2 = sum(Rnew(:) .^ 2);
  beta = rs2 / rs1;
  P = Rnew + beta * P;
  R = Rnew;
  rms = sqrt(rs1 / (n1 * n2)); 
  disp([it log10(rms)])
  if rms < delta
    break
  end
end
