% Illustrate penalty on ringing in one-dimensional deconvolution

% Simulate data
m = 50;
y = zeros(m , 1);
y(6:20) = 1;
y(31:45) = 1;

% Compute PSF
m = length(y);
fsup = 4;
n = fsup * m;
sigma0 = 0.5;
sigma = sigma0 * fsup;
Sn = psfmat(n, sigma);
S = kron(eye(m), ones(1, fsup)) * Sn;

% Exlore series of kappas and lambdas
P0 = eye(n);
D = diff(eye(n));
P1  = D' * D;
kappas = [0.1, 0.3, 1, 3];
lambdas = [0, 3, 10, 30];
nk = length(kappas);
nl = length(lambdas);
ip = 0;
for kappa = kappas
    for lambda = lambdas
        
        % Solve the equations
        Q = S' * S + kappa * P0 + lambda * P1;
        x = Q \ (S' * y);
        
        % Plot results
        um = ((1:m)' - 0.5) / m;
        un = ((1:n)' - 0.5) / n;
        ip = ip + 1;
        subplot(nk, nl, ip)
        plot(um, y)
        hold on
        plot(un, x * n / m, 'r')
        hold off
        ttl = ['kappa = ', num2str(kappa), ', lambda = ', num2str(lambda)];
        title(ttl)
        ylim([-0.5 1.5])
    end
end


