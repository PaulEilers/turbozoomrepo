function [F, A, Bc, Br] = turbopsp(Z, Ppars, W);
% Very fast fitting of tensor P-splines to a full data matrix Z
% Weights in W
% Row 1 of Ppars: [nseg bdeg lambda pord] for columns
% Row 2 of Ppars: dito for rows

[m n] = size(Z);
if nargin < 3
    W = ones(m, n);
end

% Prepare basis and penalty for columns
cpars = [0 n+1 Ppars(2, :)];
Bc = bsplbase((1:n)', cpars);
nc = size(Bc, 2);
Dc = diff(eye(nc), cpars(6));
Pc = cpars(5) * Dc' * Dc;

% Prepare basis and penalty for rows
rpars = [0 m+1 Ppars(1, :)];
Br = bsplbase((1:m)', rpars);
nr = size(Br, 2);
Dr = diff(eye(nr), rpars(6));
Pr = rpars(5) * Dr' * Dr;

% Do the fitting
A = turbotens(Z, Bc, Br, Pc, Pr, W);
F = Br * A * Bc';
