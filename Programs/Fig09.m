% Contrast correction for DNA data

load ../Data/dataDNAmapping_subset.mat
Y = dataDNAmapping(1:100, 1:100, 1);
Y = (Y - min(Y(:))) / (max(Y(:)) - min(Y(:)));

% Compute PSF
[m1 m2] = size(Y);
fsup = 8;
n1 = fsup * m1;
n2 = fsup * m2;
sigma = 0.8* fsup;
S1a = psfmat(n1, sigma);
S2a = psfmat(n2, sigma);
S1a = psfmat(n1, sigma);
S2a = psfmat(n2, sigma);
S1 = kron(eye(m1), ones(1, fsup)) * S1a;
S2 = kron(eye(m2), ones(1, fsup)) * S2a;

% Prepare components of the linear system
U = S1' * Y * S2;
G1 = S1' * S1;
G2 = S2' * S2;
kappa = 0.1;
lambda = 10;
X = CG2(U, G1, G2, kappa, lambda, 0.001);

% Correct range and baseline
% To be added: description of parameters 
tic;
rlambda = 0.1;
clambda = rlambda;
rpars = [20 2 rlambda 1];
cpars = [20 2 clambda 1];
pars = [rpars; cpars];
Fhi = asypsp2(X, 0.99, pars);
Flo = asypsp2(X, 0.01, pars);
toc
S = max(Fhi - Flo, 0.001);
G = (X - Flo) ./ S; 

% Correct original image
Fhi = asypsp2(Y, 0.99, pars);
Flo = asypsp2(Y, 0.01, pars);
S= max(Fhi - Flo, 0.0);
Yc = (Y - Flo) ./ S; 

figure(1)
subplot(2, 2, 1)
imagesc(Y);
C = flipud(colormap(gray));
colormap(C)
title('Selection')

subplot(2, 2, 2)
imagesc(Yc);
title('Selection, corrected')

subplot(2, 2, 3)
imagesc(X);
title('Zoomed')

subplot(2, 2, 4)
imagesc(G);
title('Zoomed with correction')




