% SuperZoomer for DNA image with contrast enhancement

% Get the data and normalize
load('../Data/MFdata_subset.mat');
Y0 = data;
Y0 = (Y0 - min(Y0(:))) / (max(Y0(:)) - min(Y0(:)));

% Select a rectangle
i1 = 1;
i2 = 100;
j1 = 91;
j2 = 190;
Y = Y0(j1:j2, i1:i2);

% Renormalize selection
Y = (Y - min(Y(:))) / (max(Y(:)) - min(Y(:)));

% Compute PS, set fsup to super-resolution factor
fsup = 8;
[n1 n2] = size(Y);
p1 = fsup * n1;
p2 = fsup * n2;
sigma = 0.8 * fsup;
S1a = psfmat(p1, sigma);
S2a = psfmat(p2, sigma);
S1 = kron(eye(n1), ones(1, fsup)) * S1a;
S2 = kron(eye(n2), ones(1, fsup)) * S2a;

% Initialize 
U = S1' * Y * S2;
G1 = S1' * S1;
G2 = S2' * S2;
kappa = 0.000001;
lambda = 30;
delta = 0.001;
X = CG2(U, G1, G2, kappa, lambda, delta);

% Correct range and baseline
rlambda = 1;
clambda = rlambda;
rpars = [30 2 rlambda 1];
cpars = rpars;
pars = [rpars; cpars];
Fhi = asypsp2(X, 0.99, pars);
Flo = asypsp2(X, 0.01, pars);
S = max(Fhi - Flo, 0.0);
G = (X - Flo) ./ S; 

% Show the zoomed-in image
subplot(1, 2, 1)
imagesc(X)
colormap(C)
title('Zoomed')

% Show the enhanced image
subplot(1, 2, 2)
imagesc(G)
colormap(C)
title('Zoomed & enhanced')


 