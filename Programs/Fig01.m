% Illustrate deconvolution

% Simulate data
m = 40;
y = sin(2* pi * ((1:m) -0.5)'/ 10); 

% Compute PSF
m = length(y);
fsup = 4;
n = fsup * m;
sigma0 = 0.5;
sigma = sigma0 * fsup;
Sn = psfmat(n, sigma) / fsup;
S = kron(eye(m), ones(1, fsup)) * Sn;

% Penalty marices 
E = eye(n);
kappa1 = 1e-2;
x1 = (S' * S + kappa1 * E) \ (S' * y);
kappa2 = 1e-1;
x2 = (S' * S + kappa2 * E) \ (S' * y);

% Plot data and results
plot((1:m) - 0.5, y, 'sk')
hold on 
plot((1:n) / fsup, x1, '.b')
plot((1:n) / fsup, x2, '.r')
hold off
title('Super-resolution of a sine wave') 


