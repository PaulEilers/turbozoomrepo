function S = psfmat(n, sigma)
% Create a point spread matrix with width sigma and length n

u = -n:n;
v = exp(-(u' / sigma) .^ 2 / 2);
v = v / sum(v);
S = zeros(n, n);
nv = length(v);
for i = 1:n
    l = max(i - n, 1);
    u = min(i + n, n);
    l2 = max(l - i + n + 1, 1);
    u2 = min(u - i + n + 1, nv);
    S(i, l : u) = v(l2 : u2);
end
