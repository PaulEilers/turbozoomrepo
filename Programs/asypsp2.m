function Z = asypsp2(Y, p, pars)
W = 0 * Y + 0.5;
for it = 1:10
   Z = turbopsp(Y, pars, W);
   R = Y - Z;
   Wold = W;
   W = p * (R > 0) + (1 - p) * (R <= 0);
   sw = sum(W(:) ~= Wold(:));
   if sw == 0
     break
   end
end
