% SuperZoomer fior DNA image
% Show influence of convergence criterium

% Get the data and normalize
load('../Data/dataDNAmapping_subset.mat');
Y0 = dataDNAmapping;
Y = Y0;

% Renormalize selection
Y = (Y - min(Y(:))) / (max(Y(:)) - min(Y(:)));

% Show original image
close all
figure(1)
nf1 = 2;
nf2 = 3;
subplot(nf1, nf2, 1)
imagesc(Y0)
C = flipud(colormap(gray));
colormap(C)
title('Original')

% Show the selection
subplot(nf1, nf2, 2)
imagesc(Y)
colormap(C)
title('Selection')
ip = 2;

% Compute PSF
tic;
[n1 n2] = size(Y);
fsup = 8;
p1 = fsup * n1;
p2 = fsup * n2;
sigma = 0.8* fsup;
S1a = psfmat(p1, sigma);
S2a = psfmat(p2, sigma);
S1a = psfmat(p1, sigma);
S2a = psfmat(p2, sigma);
S1 = kron(eye(n1), ones(1, fsup)) * S1a;
S2 = kron(eye(n2), ones(1, fsup)) * S2a;

% Initialize
U = S1' * Y * S2;
G1 = S1' * S1;
G2 = S2' * S2;
kappa = 0.000001;
lambda = 30;
deltas = [0.01, 0.003, 0.001, 0.0001];
for delta = deltas
  [X, nit] = CG2(U, G1, G2, kappa, lambda, delta);
  toc;
  
  % Show the zoomed-in image
  ip = ip + 1;
  subplot(nf1, nf2, ip)
  imagesc(X)
  colormap(C)
  title([num2str(nit), ' steps (', num2str(delta), ')'] )
end



