function A = psp2turbo(Z, Bc, Br, Pr, Pc, W);
% Very fast fitting of tensor P-splines to a full data matrix Z
% Weights in W
% Basis and penalty matrix for columns in Bc and Pc
% Basis and penalty matrix for rows in Br and Pr

[m n] = size(Z);
if nargin < 6
    W = ones(m, n);
end

nc = size(Bc, 2);
nr = size(Br, 2);
T = rowtens(Br, Br)' * W * rowtens(Bc, Bc);
T = reshape(T, [nr nr nc nc]);
T = permute(T, [1 3 2 4]);
T = reshape(T, nr * nc, nr * nc) + kron(Pc, eye(nr)) + kron(eye(nc), Pr);
s = reshape(Br' * (W .* Z) * Bc, nr * nc, 1);
A = reshape(T \ s,  nr, nc);
