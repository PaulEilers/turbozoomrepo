### Introduction
This repository contains program and data to accompany the paper
"Fast and Simple Super-resolution with Single Images", by Paul Eilers and Cyril Ruckebusch, 
that will appear in *Scientific Reports*. As the title says, we offer a fast deconvolution algorithm for single (fluorescence) 
images. The code is written in Matlab.

### Installation and use
No special installation is needed and no special Matlab toolboxes are being used. Just clone this repsitory and start 
working in the directory *Programs*. The programs were tested on Matlab versions 2012a and ...

### Reproduce the figures in the paper.
The programs with names *Fig01.m* to  *Fig10.m* reproduce the figures with the same numbers. They depend on several Matlab functions in this repository.

## About the data
We do not have permission to publish the complete Maize and DNA images here. Only subsets are included. 
That means that, in contrast to the paper, for several figures the original image and the selection are identical.

### Contacts
Paul Eilers (*p.eilers@erasmusmc.nl*)
Cyril Ruckebusch (*cyril.ruckebusch@univ-lille.fr*)

### The paper
The link to the paper will appear here in due time.